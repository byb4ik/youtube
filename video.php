<?php

/**
 * Library Requirements
 *
 * 1. Install composer (https://getcomposer.org)
 * 2. On the command line, change to this directory (api-samples/php)
 * 3. Require the google/apiclient library
 *    $ composer require google/apiclient:~2.0
 */
if (!file_exists(__DIR__ . '/vendor/autoload.php')) {
    throw new \Exception('please run "composer require google/apiclient:~2.0" in "' . __DIR__ . '"');
}

require_once __DIR__ . '/vendor/autoload.php';


// This code will execute if the user entered a search query in the form
// and submitted the form. Otherwise, the page displays the form above.

    /*
     * Set $DEVELOPER_KEY to the "API key" value from the "Access" tab of the
     * {{ Google Cloud Console }} <{{ https://cloud.google.com/console }}>
     * Please ensure that you have enabled the YouTube Data API for your project.
     */
    $DEVELOPER_KEY = 'AIzaSyDVMSTiXF0JhSpikydvjTPEGIwWJNvYdr8';

    $client = new Google_Client();
    $client->setDeveloperKey($DEVELOPER_KEY);

    // Define an object that will be used to make all API requests.
    $youtube = new Google_Service_YouTube($client);



    // Call the search.list method to retrieve results matching the specified
    // query term.
    $searchResponse = $youtube->search->listSearch('id,snippet', [
        'q' => $query,
        'maxResults' => 25,
    ]);
    //массив для записей (пара [id video] => [title])
    $arr = [];
    // Add each result to the appropriate list, and then display the lists of
    // matching videos, channels, and playlists.
    foreach ($searchResponse['items'] as $searchResult) {
//var_dump($searchResult['snippet']['thumbnails']['high']['url']);
        if (!empty($searchResult['id']['videoId'])) {
            $date_video = date("F j, Y, g:i a");
            $db = new DB();
            $db->AddVideo($searchResult['id']['videoId'], $searchResult['snippet']['title'], $searchResult['snippet']['thumbnails']['high']['url'], $date_video);
        }
    }



?>
