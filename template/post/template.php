﻿<?php foreach ($data as $video){ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru-RU">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		

					<title><?php echo $video->title_video ?></title>
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />


		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/[HOST]/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b===c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='redwaves-lite-stylesheet-css' href='/template/post/wp-content/themes/redwaves-lite/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css' href='/template/post/wp-content/themes/redwaves-lite/css/font-awesome.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='redwaves-lite-roboto-googlefont-css' href='https://fonts.googleapis.com/css?family=Roboto:400italic,300,700,400' type='text/css' media='all' />
<link rel='stylesheet' id='redwaves-lite-slidebars-css' href='/template/post/wp-content/themes/redwaves-lite/css/slidebars.min.css' type='text/css' media='all' />
<script type='text/javascript' src='/template/post/wp-includes/js/jquery/jquery.js'></script>
<script type='text/javascript' src='/template/post/wp-includes/js/jquery/jquery-migrate.min.js'></script>
<script type='text/javascript' src='/template/post/wp-content/themes/redwaves-lite/js/slidebars.min.js'></script>
<link rel='https://api.w.org/' href='/template/post/wp-json/' />

 
<meta name="generator" content="WordPress 4.8" />
	<style type="text/css">
		 button, .pagination a, .nav-links a, .readmore, .thecategory a:hover, .pagination a, #wp-calendar td a, #wp-calendar caption, #wp-calendar #prev a:before, #wp-calendar #next a:before, .tagcloud a:hover, #wp-calendar thead th.today, #wp-calendar td a:hover, #wp-calendar #today { background: #c60000; } .secondary-navigation, .secondary-navigation li:hover ul a, .secondary-navigation ul ul li, .secondary-navigation ul ul li:hover, .secondary-navigation ul ul ul li:hover, .secondary-navigation ul ul ul li, #mobile-menu-wrapper, a.sideviewtoggle, .sb-slidebar { background: #c60000; }  .thecategory ul li a:hover { background: #c60000; !important} a, .breadcrumb a, .entry-content a {color: #c60000;} .title a:hover, .post-data .post-title a:hover, .post-title a:hover, .post-info a:hover,.textwidget a, .reply a, .comm, .fn a, .comment-reply-link, .entry-content .singleleft a:hover, .breadcrumb a:hover, .widget-post-title a:hover { color: #c60000; } .main-container .widget h3:after, .tagcloud a:hover { border-color: #c60000; }  body { background: #f7f7f7;} article, .sidebar-widget, .related-posts .horizontal-container, .author-box, .error404 .content-area { -webkit-box-shadow: 0px 1px 1px #c2c4c4; -moz-box-shadow: 0px 1px 1px #c2c4c4; box-shadow: 0px 1px 1px #c2c4c4; } 	</style>

<meta itemprop="name" content="[KEYWORDURL]">
<meta itemprop="description" content="[DESCRIPTION]">
<meta itemprop="summary" content="[DESCRIPTION]">
<meta itemprop="ratingValue" content="5">
<meta itemprop="itemreviewed" content="[BKEYWORD]">

</head>
	<body class="home blog">
		<div id="page sb-site" class="hfeed site">
			<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
						<header id="masthead" class="site-header" role="banner">
				<div class="container header-container sb-slide">
					<div class="header-inner">
						<div class="logo-wrap">

									</div><!-- .logo-wrap -->
						<div class="header_area-wrap">
									<div class="header_area">
					</div><!-- .header_area -->
								</div><!-- .header_area-wrap -->
					</div><!-- .header-inner -->
				</div><!-- .container -->
				<div id="sideviewtoggle" class="secondary-navigation sb-slide">

				</div>
				<div id="sticky" class="secondary-navigation">
					<div class="container clearfix">
						<nav id="site-navigation" class="main-navigation" role="navigation">
															<div class="no-menu-msg"><?php echo $video->title_video ?></div>
														
						</nav><!-- #site-navigation -->
					</div><!--.container -->
				</div>	
			</header><!-- #masthead -->
			<div id="content" class="main-container sb-slide"><div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">


				 <article id="post-[N]" class="post-[N] post type-post status-publish format-standard hentry ">
	<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#"><div>
            <i class="fa fa-home"></i></div> <div typeof="v:Breadcrumb" class="root">
            </div>



 </div>
	<header class="entry-header">
		<h1 class="entry-title"><?php echo $video->title_video; ?></h1>
        <div class="entry-meta post-info">		<span class="thecategory">
					</span>


	<span class="theauthor"><i class="fa fa-user"></i> <span class="author vcard"><span class="url fn"> </span></span></span>
            <span class="posted"><i class="fa fa-clock-o"></i>
                <time class="entry-date published" datetime="[TIMEFORMAT-(s)]+00:00"><?php echo $video->date_video; ?></time>

            </span>
           <br />
			</div><!-- .entry-meta -->
				</header><!-- .entry-header -->
	<div class="entry-content"><center>
       

        <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $video->id_video; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br>
           
           </center>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
        <?php } ?>
			<div class="next_prev_post">
							</div>
	 	</main><!-- #main -->
</div><!-- #primary -->

<div id="secondary" class="widget-area" role="complementary">
	<aside id="search-2" class="widget sidebar-widget widget_search"><form role="search" method="get" class="search-form" action="/">
    <div>
    <span class="screen-reader-text">Search for</span>
    <input type="search" class="search-field" placeholder="Search &#8230;" value="" name="s" title="Search for:" />
	<button type="submit" class="search-submit">
		<i class="fa fa-search"></i>
	</button>	
 </div>
</form></aside>		<aside id="recent-posts-2" class="widget sidebar-widget widget_recent_entries">		<h3 class="widget-title">Свежие записи</h3>		<ul>
<?php

foreach ($data1 as $video1){ ?>
<a class="next page-numbers" href="/post.php?id=<?php echo $video1->id_video; ?>">
    <img src="<?php echo $video1->pics_video; ?>">
    <br>
    <h4><?php echo $video1->title_video ?></h4>
    </a>
<?php } ?>

</div><!-- #secondary -->
</div><!--/.main-container -->
<footer id="colophon" class="site-footer sb-slide" role="contentinfo">
	<div class="footersep"></div>
	<div class="copyrights">

	</div>
</footer>
</div><!--/#page -->

<div class="sb-slidebar sb-left sb-width-custom sb-style-overlay" data-sb-width="250px">
	<div id="mobile-menu-wrapper">

					<div class="mobile_search">
 		</div>
				<nav id="navigation" class="clearfix">
			<div id="mobile-menu" class="mobile-menu">

							</div>
		</nav>							
	</div>
</div>
<div class="obfuscator sb-toggle-left"></div>

<script type='text/javascript' src='/template/post/wp-content/themes/redwaves-lite/js/jquery.pin.js'></script>
<script type='text/javascript' src='/template/post/wp-includes/js/wp-embed.min.js'></script>
        <!--LiveInternet counter--><script type="text/javascript">
            document.write("<a href='//www.liveinternet.ru/click;videosite' "+
                "target=_blank><img src='//counter.yadro.ru/hit;videosite?t45.6;r"+
                escape(document.referrer)+((typeof(screen)=="undefined")?"":
                    ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                    screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                ";h"+escape(document.title.substring(0,150))+";"+Math.random()+
                "' alt='' title='LiveInternet' "+
                "border='0' width='31' height='31'><\/a>")
        </script><!--/LiveInternet-->

    </body>
</html>	