<?php
define('DB_NAME', 'db/video.db');
if(!file_exists(DB_NAME)) {
    $db = new SQLite3(DB_NAME);
    $sql = "CREATE TABLE video (
            id INTEGER PRIMARY KEY,
            id_video STRING UNIQUE,
            title_video TEXT,
            pics_video STRING,
            date_video STRING
        )";
    $db->query($sql);
    echo 'База ' . DB_NAME . ' создана';
}else
{
    echo 'База ' . DB_NAME . ' уже существует';
}
?>