<?php


class DB extends \SQLite3
{
    protected $dbh;

    public function __construct()
    {
		$path_to_base = __DIR__ . '/db/video.db';
        $this->dbh = new \PDO('sqlite:' . $path_to_base);
    }

    public function execute($sql, $data = [])
    {
        $sth = $this->dbh->prepare($sql);
        return $sth->execute($data);
    }

    public function LastRecord()
    {
        $sql = "SELECT * FROM video ORDER BY id DESC LIMIT 10";
        $sth = $this->dbh->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll(\PDO::FETCH_CLASS);
        return $data;
    }

    public function getVideo($id_video)
    {
        $sql = "SELECT * FROM video WHERE id_video = :id_video";

        $sth = $this->dbh->prepare($sql);
        $sth->execute([':id_video' => $id_video]);
        $data = $sth->fetchAll(\PDO::FETCH_CLASS);
        return $data;
    }

    public function AddVideo($id_video, $title_video, $pics_video, $date_video)
    {
 
        $sql = "INSERT INTO video ( id_video, title_video, pics_video, date_video)
                VALUES ( '$id_video', '$title_video', '$pics_video', '$date_video' )";
		$sth = $this->dbh->prepare($sql);
		
        return $sth->execute();
    }
}